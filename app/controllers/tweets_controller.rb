class TweetsController < ApplicationController
  def index
    @tweets = Tweet.all
  end
  
  def new
    @tweet = Tweet.new
  end
  
  def create
    message = params[:tweet][:message].read
    tweet = tweet.new(message: message, tdate: params[:tweet][:tdate])
    if tweet.save
      flash[:notice] = '1レコード追加しました'
      redirect_to '/'
    else
      render 'new'
    end
  end
  
  def destroy
    tweet = Tweet.find(params[:id])
    tweet.destroy
    redirect_to '/'
  end
  
  def edit
    @tweets = Tweet.find(params[:id])
  end
  
  def update
    message = params[:tweet][:message].read
    tweet = Tweet.find(params[:id])
    tweet.update(message: message, tdate: params[:tweet][:tdate])
    redirect_to '/'
  end
end
